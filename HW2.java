package HW.HW2;

import java.util.Scanner;
import java.util.Random;

public class HW2 {
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        String[][] vector = new String[5][5];

        int hor = 0;
        int ver = 0;
        boolean res = true;

        System.out.println("0   1   2   3   4   5 ");
        while (hor < 5) {
            System.out.print(hor + 1);
            while (ver < 5) {
                vector[hor][ver] = " | -";
                System.out.print(vector[hor][ver]);
                ver++;
            }
            System.out.println(" |");
            ver = 0;
            hor++;
        }
        int inputX = rand.nextInt(1, 6);
        int inputY = rand.nextInt(1, 6);
        System.out.println("ЦЕЛЬ ДЛЯ ПРОВЕРКИ " + inputX + "-" + inputY);

        while (res) {
            while (res) {
                int userX;
                int userY;
                while (true) {
                    System.out.print("enter horizontal number (1-5) : ");
                    userX = sc.nextInt();
                    if (1 <= userX & userX <= 5) {
                        break;
                    } else {
                        System.err.println("Number not within 1 to 5,write new number: 1 - 5");
                    }
                }

                while (true) {
                    System.out.print("enter vertical number (1-5) : ");
                    userY = sc.nextInt();
                    if (1 <= userY & userY <= 5) {
                        break;
                    } else {
                        System.err.println("Number not within 1 to 5,write new number: 1 - 5");
                    }
                }
                if (inputX == userX & inputY == userY) {
                    vector[userY - 1][userX - 1] = new String(" | X");
                    System.out.println("You have won!");
                    System.out.println("0   1   2   3   4   5 ");
                    hor = 0;
                    while (hor < 5) {
                        System.out.print(hor + 1);
                        while (ver < 5) {
                            System.out.print(vector[hor][ver]);
                            ver++;
                        }
                        System.out.println(" |");
                        ver = 0;
                        hor++;
                        res = false;
                    }
                } else {
                    vector[userY - 1][userX - 1] = new String(" | *");
                    System.out.println("0   1   2   3   4   5 ");
                    hor = 0;
                    while (hor < 5) {
                        System.out.print(hor + 1);
                        while (ver < 5) {
                            System.out.print(vector[hor][ver]);
                            ver++;
                        }
                        System.out.println(" |");
                        ver = 0;
                        hor++;
                    }
                }
            }
        }
    }
}


