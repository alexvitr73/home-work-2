package HW;

import java.util.Random;
import java.util.Scanner;

public class HW1 {
    public static void main(String[] args) {
        System.out.println("Let the game begin!");
        System.out.print("Input your name: ");
        Scanner scan = new Scanner(System.in);
        String name = scan.nextLine();

        Random random = new Random();
        int number = random.nextInt(101);

        while (true) {
            System.out.println("Input number from 0 100");
            int inputNumber = scan.nextInt();
            if (inputNumber == number) {
                System.out.println("Congratulations, " + name + " !");
                break;
            } else if (inputNumber < number) {
                System.out.println("Your number is too small. Please, try again.");
            } else {
                System.out.println("Your number is too big. Please, try again.");
            }
        }
    }
}